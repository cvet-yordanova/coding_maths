var particle = {
    position: null,
    velocity:null,
    mass: 1,
    radius: 0,
    bounce: -1,
    gravity: 0,

    create: function(x, y, speed, direction, grav) {
        var obj = Object.create(this);
        obj.position = {x: 3, y: 3}
        obj.velocity = {x: 5, y: 5}
        obj.velocity.length = 3;
        obj.velocity.angle = 15;
        obj.gravity = {x: 0, y: 1};

        return obj;

    },

    update: function () {
        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
    }


}




window.onload = function() {
    var canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    width = canvas.width = window.innerWidth,
    height = canvas.height = window.innerHeight;
    //create particle
    p = particle.create(width/2, height / 2, 5, Math.random() * Math.PI * 2, 0.1);

    p.radius = 40;
    p.bounce = -1;

    update();

    function update() {
        context.clearRect(0, 0, width, height);

        p.update();
        context.beginPath();
        context.arc(p.position.x, p.position.y, p.radius, 0, Math.PI * 2, false);
        context.fill();

        if(p.position.x + p.radius > width) {
            p.position.x = width - p.radius;
            p.velocity.x = p.velocity.x * p.bounce;
        }

        if(p.position.x - p.radius < 0) {
            p.position.x = p.radius;
            p.velocity.x = p.velocity.x * p.bounce;
        }

        if(p.position.y + p.radius > height) {
			p.position.y = height - p.radius;
            p.velocity.y = p.velocity.y * p.bounce;
		}

        if(p.position.y - p.radius < 0) {
			p.position.y = p.radius;
            p.velocity.y = p.velocity.y * p.bounce;
		}

        requestAnimationFrame(update);
    }
}

