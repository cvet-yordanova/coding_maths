

window.onload = function () {
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');


    let LEFT, UP, RIGHT, DOWN;
    let velocity = 4;
    let friction = 0.1;
    const BALLZ = [];

    class Vector {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }

        add(v) {
            this.x += v.x;
            this.y += v.y;
        }

        subtr(v) {
            this.x -= v.x;
            this.y -= v.y;
        }

        mag() {
            return Math.sqrt(this.x ** 2 + this.y ** 2);
        }

        mult(n) {
            this.x *= n;
            this.y *= n;
        }

       static dot(v1, v2) {
        return v1.x*v2.x + v1.y*v2.y;
       }

        unit() {
            if (this.mag() === 0) {
                return new Vector(0, 0);
            } else {
                return new Vector(this.x / this.mag(), this.y / this.mag())
            }
        }

        normal() {
            return new Vector(-this.y, this.x).unit();
        }

        drawVec(start_x, start_y, n, color) {
            ctx.beginPath();
            ctx.moveTo(start_x, start_y);
            ctx.lineTo(start_x + this.x * n, start_y + this.y * n);
            ctx.strokeStyle = color;
            ctx.stroke();
        }
    }


    class Ball {
        constructor(x, y, r) {
            this.pos = new Vector(x, y);
            this.vel = new Vector(0, 0);
            this.acceleration = 1;
            this.acc = new Vector(0, 0);
            this.r = r;
            BALLZ.push(this);

        }

        drawBall() {
            ctx.beginPath();
            ctx.arc(this.pos.x, this.pos.y, this.r, 0, 2 * Math.PI);
            ctx.strokeStyle = 'black';
            ctx.stroke();
            ctx.fillStyle = 'red';
            ctx.fill();
        }

        display() {
            this.vel.drawVec(550, 400, 10, 'green');
            this.acc.unit().drawVec(550, 400, 100, 'blue');
            ctx.beginPath();
            ctx.arc(550, 400, 50, 0, 2 * Math.PI);
            ctx.strokeStyle = 'black';
            ctx.stroke();
        }

        reposition() {
            this.acc = this.acc.unit();
            this.acc.unit().mult(this.acceleration)
            this.vel.add(this.acc);
            this.vel.mult(1 - friction);
    
            this.pos.add(this.vel);
        }


    }

    let Ball1 = new Ball(200, 300, 30);
    let Ball2 = new Ball(300, 250, 40);


    canvas.addEventListener('keydown', function (e) {

        if (e.code === 'ArrowLeft') {
            LEFT = true;
        }
        if (e.code === 'ArrowUp') {
            UP = true;
        }
        if (e.code === 'ArrowRight') {
            RIGHT = true;
        }
        if (e.code === 'ArrowDown') {
            DOWN = true;
        }
    });

    canvas.addEventListener('keyup', function (e) {

        if (e.code === 'ArrowLeft') {
            LEFT = false;
        }
        if (e.code === 'ArrowUp') {
            UP = false;
        }
        if (e.code === 'ArrowRight') {
            RIGHT = false;
        }
        if (e.code === 'ArrowDown') {
            DOWN = false;
        }
    });


    function keyControl(b) {
        if (LEFT) {
            b.acc.x = -b.acceleration;
        }
        if (UP) {
            b.acc.y = -b.acceleration;
        }
        if (RIGHT) {
            b.acc.x = b.acceleration;
        }
        if (DOWN) {
            b.acc.y = b.acceleration;
        }


        if (!UP && !DOWN) {
            b.acc.y = 0;
        }

        if (!RIGHT && !LEFT) {
            b.acc.x = 0;
        }


     
    }

    let distanceVec = new Vector(0, 0);

    function round(number, precision) {
        let factor = 10 ** precision;
        return Math.round(number * factor) / factor;
    }
    let balldiff;

    function coll_det_bb(b1, b2) {
        let dist = new Vector(b1.pos.x - b2.pos.x, b1.pos.y - b2.pos.y);
        if (b1.r + b2.r >= dist.mag()) {
            return true;
        } else {
            return false;
        }
    }

    function pen_res_bb(b1, b2) {
        let dist = new Vector(b1.pos.x - b2.pos.x, b1.pos.y - b2.pos.y);
        let pen_depth = b1.r + b2.r - dist.mag();

        let pen_res = dist.unit();
        pen_res.mult(pen_depth / 2);
        console.log(pen_depth)
        b1.pos = new Vector(b1.pos.x + pen_res.x, b1.pos.y + pen_res.y);
        b2.pos = new Vector(b2.pos.x + (pen_res.x * -1), (b2.pos.y + (pen_res.y * -1)));
    }

    function coll_res_bb(b1, b2) {
        let normal = new Vector(b1.pos.x - b2.pos.x, b1.pos.y - b2.pos.y).unit();
        let relVel = new Vector(b1.vel.x - b2.vel.x, b1.vel.y - b2.vel.y)
        let sepVel = Vector.dot(relVel, normal);
        let new_sepVel = -sepVel;
        let sepVelVec = new Vector(normal.x*new_sepVel, normal.y*new_sepVel);
    
        b1.vel = new Vector(b1.vel.x + sepVelVec.x, b1.vel.y + sepVelVec.y);
        b2.vel = new Vector(b2.vel.x + (sepVelVec.x * -1), (b2.vel.y + (sepVelVec.y * -1)));

    }

    update();

    function update() {
        ctx.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);

        keyControl(Ball1);

        BALLZ.forEach((b) => {
            b.drawBall();
            b.display();
            b.reposition();
        });

        if (coll_det_bb(Ball1, Ball2)) {
            pen_res_bb(Ball1, Ball2);
            coll_res_bb(Ball1, Ball2);
        }


        distanceVec = new Vector(Ball1.pos.x - Ball2.pos.x, Ball1.pos.y - Ball2.pos.y);
        ctx.fillText('distance:' + distanceVec.mag(), 500, 330)
        requestAnimationFrame(update);
    }


}